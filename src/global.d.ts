// necessary to import style as objects in ts files
declare module '*.scss' {
  const content: { [className: string]: string };
  export = content;
}