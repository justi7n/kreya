import React from "react";
import Layout from "@theme/Layout";
import useDocusaurusContext from "@docusaurus/useDocusaurusContext";
import Hero from "../components/Hero";
import FeatureCard from "../components/FeatureCard";
import {
  faArrowsAltH,
  faCheck,
  faCode,
  faSignInAlt,
  faTimes,
} from "@fortawesome/free-solid-svg-icons";
import { faGitAlt } from "@fortawesome/free-brands-svg-icons";
import Container from "../components/Container";

const features = [
  {
    title: "gRPC Client",
    icon: faArrowsAltH,
    description:
      "The ultimative gRPC client with support for server reflection, streamed requests, request validation and more.",
  },
  {
    title: "Environments & Templating",
    icon: faCode,
    description: "Environments and templating allow dynamic requests.",
  },
  {
    title: "File based",
    icon: faGitAlt,
    description:
      "All requests and configurations are stored in the file system. Sync your Kreya projects with git or any other syncing software.",
  },
  {
    title: "Authentication",
    icon: faSignInAlt,
    description:
      "Define your authentication values in one place and reference them in requests. No need to repeat yourself.",
  },
];

const featureComparison = {
  products: [
    {
      name: "Kreya",
    },
    {
      name: "BloomRPC",
    },
    {
      name: "Insomnia",
    },
    {
      name: "Wombat",
    },
  ],
  features: [
    {
      name:
        "Unary, client streaming, server streaming & duplex streaming calls",
      support: [true, true, true, true],
    },
    {
      name: "gRPC server reflection",
      support: [true, false, false, true],
    },
    {
      name: "Proto file discovery",
      support: [true, false, true, true],
    },
    {
      name: "Define once, use everywhere authentication configurations",
      support: [true, false, false, false],
    },
    {
      name: "File based storage for easy syncing",
      support: [true, false, false, false],
    },
    {
      name: "Environments & Templating",
      support: [true, true, true, false],
    },
    {
      name: "gRPC Web support",
      support: [true, true, false, false],
    },
    {
      name: "Define default settings per project or directory",
      support: [true, false, false, false],
    },
  ],
};

export default function Home(): JSX.Element {
  const { siteConfig } = useDocusaurusContext();
  return (
    <Layout
      title={`Hello from ${siteConfig.title}`}
      description="Description will go into a meta tag in <head />"
    >
      <Hero
        title={siteConfig.title}
        tagline={siteConfig.tagline}
      />

      <Container>
        <div className="grid grid-flow-row md:grid-flow-col auto-cols-fr gap-3 mb-6">
          {features.map((f) => (
            <FeatureCard key={f.title} {...f} />
          ))}
        </div>
      </Container>
    </Layout>
  );
}
