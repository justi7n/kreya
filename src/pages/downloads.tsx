import React from 'react';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import styles from './index.module.scss';


const downloads = [
  {
    title: 'macOS (x64)',
    os: 'macos',
    link: 'https://stable-downloads.kreya.app/Kreya-macos.zip',
    checksumLink: 'https://stable-downloads.kreya.app/Kreya-macos-zip-checksum-sha256.txt',
  },
  {
    title: 'Windows (x64)',
    os: 'windows',
    link: 'https://stable-downloads.kreya.app/Kreya-win.msi',
    checksumLink: 'https://stable-downloads.kreya.app/Kreya-win-msi-checksum-sha256.txt',
  },
  {
    title: 'Linux (x64, snapcraft)',
    os: 'Linux',
    link: 'https://snapcraft.io/kreya',
  },
  {
    title: 'Linux (x64, tar.gz)',
    os: 'Linux',
    link: 'https://stable-downloads.kreya.app/Kreya-linux.tar.gz',
    checksumLink: 'https://stable-downloads.kreya.app/Kreya-linux-tar-gz-checksum-sha256.txt',
  },
];

export default function Downloads(): JSX.Element {
    const {siteConfig} = useDocusaurusContext();
    return (
      <Layout
        title={`Hello from ${siteConfig.title}`}
       description="Description will go into a meta tag in <head />">
      <main>
      <div className="flex flex-col text-center">
      <h1 className="mx-auto">Download Kreya</h1>
      <p className="mx-auto mb-4">Kreya is free during beta (and may continue to be free afterwards). No credit card or login required.</p>
      <div className="flex space-x-2 justify-center">
        {downloads.map((d) => (
          <div key={d.link}>
            <a href={d.link} className="button flex space-x-1">
              <span>{d.title}</span>
            </a>
            {d.checksumLink ? <a href={d.checksumLink}>Checksum</a> : <></>}
          </div>
        ))}
      </div>

      <p className="mx-auto mt-4 italic">
        Note: On Windows, the installer downloads <a href="https://docs.microsoft.com/en-us/microsoft-edge/webview2/">Microsoft Edge WebView2</a> (~90MB) if it is not already
        installed.
      </p>
      <p className="mx-auto italic">
        When installing Kreya on Linux via tarball, both <code>libgtk-3</code> and <code>libwebkit2gtk-4.0</code> are required.
      </p>
    </div>
      </main>
    </Layout>


    );
        }   

