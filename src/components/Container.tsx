import React from "react";

interface ContainerProps {
  variant?: 'default' | 'full-width'
  children: JSX.Element;
}

export default function Container({
  variant,
  children,
}: ContainerProps): JSX.Element {
  return (
    <div className="container">
      {children}
    </div>
  );
}