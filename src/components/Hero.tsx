import React from "react";
import clsx from "clsx";
import styles from "./Hero.module.scss";

interface HeroProps {
  title: string;
  tagline: string;
  description?: JSX.Element;
}

export default function Hero({ title, tagline, description }: HeroProps): JSX.Element {
  return (
    <section className={clsx("bg-accent-400 hero", styles.heroBanner, "mb-2")}>
      <div className="container">
        <h1 className="hero__title mb-3">{title}</h1>
        <p className="hero__subtitle">{tagline}</p>
        <p className="mt-4">{description}</p>
      </div>
    </section>
  );
}
