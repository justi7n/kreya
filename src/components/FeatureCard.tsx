import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconDefinition } from '@fortawesome/fontawesome-common-types';

interface FeatureCardProps {
  title: string;
  description: string;
  icon: IconDefinition;
}

export default function FeatureCard({
  title,
  description,
  icon,
}: FeatureCardProps): JSX.Element {
  return (
    <div className="bg-gray-300 rounded p-2">
      <div className="text-xl mb-2 flex justify-center items-center space-x-2">
        <FontAwesomeIcon icon={icon} />
        <h2 className="inline text-xl m-0">{title}</h2>
      </div>
      <p>{description}</p>
    </div>
  );
}
