---
sidebar_position: 1
title: Getting Started
---

import useBaseUrl from '@docusaurus/useBaseUrl';

Follow these steps to call your first gRPC API using Kreya.

## Download and install Kreya

You can download Kreya by visiting the [download page](/downloads).

### Mac

Download the zip file and extract it. Drag the `Kreya.app` file into your Applications folder.

_Apple Silicon (M1) is not yet supported. More information in [this GitHub issue](https://github.com/riok/Kreya/issues/7)._

### Windows

Download the MSI file and run it, which should automatically install Kreya. Should you receive a Windows Defender Smart Screen warning, click on "More info" and then "Run anyway". This may happen because Windows doesn't trust the Kreya code signature enough yet.

_Kreya uses [Microsoft Edge WebView2](https://docs.microsoft.com/en-us/microsoft-edge/webview2/), which will be installed when you run the MSI (if it is not already installed)._

### Linux

The simplest option to install Kreya on Linux is via Snapcraft. Head over to https://snapcraft.io/kreya, select your Linux distribution and follow the guide.

Alternatively, you may install Kreya by downloading the tarball. Note that both <code>libgtk-3</code> and <code>libwebkit2gtk-4.0</code> dependencies are required. Install them manually if they aren't present on your system.

## Creating a project

Launch Kreya and click the `Create project...` button, after which you will see the following dialog:

<img alt="The create project dialog" src={useBaseUrl('/set-active-environment.png')}  width="580" height="634" />

1. Since Kreya is file-based, you need to select a location for the Kreya project.
2. Choose a name for your project.
3. Enter the directory name. It will be created inside the directory you chose in step 1. All project files will be stored inside this directory.
4. Ensure that the full path of the project matches your intentions.
5. By clicking `Create`, Kreya creates and opens the project.

## Adding protobuf files

To create gRPC requests, you need to add protobuf files. Otherwise Kreya cannot know the available services and request/response types.

In the menu, click on Project &rarr; Importers. Here, you need to create a new [Importer](importers). Enter a name of your choosing.

### Adding via gRPC server reflection

If you have a gRPC service that supports [server reflection](https://github.com/grpc/grpc/blob/master/doc/server-reflection.md), you can simply enter its URL.
If you want to try Kreya and you don't have any protobuf files, enter `https://grpcb.in:9001`.

<img alt="Adding a gRPC importer with server reflection" src={useBaseUrl('/grpc-bin-importer.png')} width="1184" height="389" />

### Adding via local protobuf files

If your protobuf files are stored locally, switch the importer type to `gRPC proto files` and then select your files.

<img alt="Adding a gRPC importer with local protobuf files" src={useBaseUrl('/proto-files-importer.png')} width="1182" height="528" />

## Creating your first operation

Save and go back to the main screen. Add a new operation by clicking the icon. Choose a descriptive name and select the desired gRPC package, service and method.

<img alt="Adding a new gRPC operation" src={useBaseUrl('/create-operation.png')} width="1177" height="424" />

## Sending your first request

You are almost finished! Click on the settings tab and enter your gRPC service URL in the `Endpoint` input field. If necessary, edit your request and then send your first request!

<img alt="Operation with a selected settings tab and a filled endpoint field" src={useBaseUrl('/entering-endpoint.png')} width="1182" height="730" />

_Enter https://grpcb.in:9001 as endpoint if you used it as server reflection endpoint._

## Good to know

Kreya has a range of features that make your life easier. Be sure to try out the [Environment](environments), [Authentication](authentication), [Default Settings](default-settings) and all the other features!
